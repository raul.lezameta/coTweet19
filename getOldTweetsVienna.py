# https://github.com/Mottl/GetOldTweets3 ==> pip install GetOldTweets3
import GetOldTweets3 as got
import os
import datetime
# fancy progressbar (https://github.com/tqdm/tqdm) ==> pip/conda install tqdm
import tqdm


def saveTweetsVienna(dateSince, dateUnitl):
    tweetCriteria = (
        got.manager.TweetCriteria()
        .setNear("Vienna, Austria")
        # .setNear("48.210033, 16.363449")  # vienna
        .setWithin("25km")
        .setSince(dateSince)
        .setUntil(dateUnitl)
        # .setMaxTweets(10)
    )

    [year, month, day] = dateSince.split("-")
    IDs = []

    tweets = got.manager.TweetManager.getTweets(tweetCriteria)

    for tweet in tweets:
        IDs.append(tweet.id)

    # fileName = f"{dateSince}---{dateUnitl}.txt"
    fileName = f"{dateSince}.txt"

    # saves only IDs. To get full metadata use tool like hydrator
    monthDir = os.path.join(os.path.dirname(__file__), "tweets", year, month)
    os.makedirs(monthDir, exist_ok=True)
    with open(os.path.join(monthDir, fileName), "w") as f:
        f.write("\n".join(IDs))


if __name__ == "__main__":
    startTime = datetime.datetime.strptime(
        "2020-05-29",
        # input("input start time (exaple 2019-03-01):"),
        "%Y-%m-%d"
    )

    endTime = datetime.datetime.strptime(
        "2020-05-31",
        # input("input end time (exaple 2019-03-01):"),
        "%Y-%m-%d"
    )

    dates = [startTime]
    while dates[-1] <= endTime:
        dates.append(dates[-1] + datetime.timedelta(days=1))

    datesString = [d.strftime("%Y-%m-%d") for d in dates[:]]

    print(f"getting Tweets from {datesString[0]} to {datesString[-2]}")
    for i in tqdm.tqdm(range(len(datesString) - 1)):
        saveTweetsVienna(datesString[i], datesString[i + 1])
        # print("saved ", datesString[i])
