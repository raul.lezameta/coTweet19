import os
import datetime
import numpy as np

# Python program to
# demonstrate merging of
# two files

# Creating a list of filenames
tweetFiles = np.array(os.listdir("tweets"))
# tweetFiles = [x for x in tweetFiles if (".txt" in x) and ("2020-03" in x)]
tweetFiles = np.array([x for x in tweetFiles if (".txt" in x) and not ("march" in x)])
tweetDates = np.array(
    [datetime.datetime.strptime(tF, "%Y-%m-%d.txt") for tF in tweetFiles]
)
tweetFilesFull = [os.path.join("tweets", tF) for tF in tweetFiles]

# Open file3 in write mode
with open(os.path.join("tweets", "march.txt"), "w") as outfile:

    # Iterate through list
    for names in tweetFilesFull:

        # Open each file in read mode
        with open(names) as infile:
            outfile.write(infile.read())

        outfile.write("\n")
